#pragma once
#include <array>
#include <cstdint>
#include <string_view>
#include <limits>
#include <charconv>
#include <cassert>
#include <concepts>
#include <ranges>
#include <algorithm>
#include <compare>
#include <gsl/gsl_assert>

#include "control_sequence_definition.hpp"
#include "format.hpp"

namespace termcontrol {

namespace rng = std::ranges;

/// Immutable string for holding control sequences.
/// The size template parameter is deduced from the given control_sequence_definition `Def`
/// and parameters are checked at compile-time against the control_sequence_definition.
template <typename CharT,
          control_sequence_definition Def,
          std::size_t N = detail::max_formatted_size_v<Def>,
          typename Traits=std::char_traits<CharT>>
struct basic_control_sequence
{
    using definition = Def;
    using traits_type = Traits;
    using value_type = CharT;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;
    using const_iterator = const CharT*;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    using const_reference = const CharT&;
    using const_pointer = const CharT*;

    using string_view_type = std::basic_string_view<CharT>;

    static constexpr std::size_t capacity = N;

    template <typename... Ts>
        requires call_signature_matches<definition, Ts...>
    constexpr explicit basic_control_sequence(Ts... parameters) noexcept
            : buffer_(), size_()
    {
        auto out = format_to<definition>(buffer_.begin(), std::forward<Ts>(parameters)...);
        size_ = std::distance(buffer_.begin(), out);
//        Ensures(size_ < capacity);
    }

    template <typename... Ts>
        requires call_signature_matches<definition, Ts...>
    constexpr explicit basic_control_sequence(detail::control_sequence_definition_tag<definition>,
                                              Ts... parameters) noexcept
            : buffer_()
            , size_()
    {
        auto out = format_to<definition>(buffer_.begin(), std::forward<Ts>(parameters)...);
        size_ = std::distance(buffer_.begin(), out);
//        Ensures(size_ < capacity);
    }

    constexpr basic_control_sequence(const basic_control_sequence& other) noexcept = default;
    constexpr basic_control_sequence& operator=(const basic_control_sequence& other) noexcept = default;

    static constexpr auto max_size() noexcept -> size_type
    { return capacity; }

    constexpr auto size() const noexcept -> size_type
    { return size_; }

    constexpr auto data() const noexcept -> const_pointer
    { return buffer_.data(); }

    constexpr auto begin() const noexcept -> const_iterator
    { return buffer_.begin(); }

    constexpr auto end() const noexcept -> const_iterator
    { return std::next(buffer_.data(), size_); }

    constexpr auto cbegin() const noexcept -> const_iterator
    { return begin(); };

    constexpr auto cend() const noexcept -> const_iterator
    { return end(); }

    constexpr auto rbegin() const noexcept -> const_reverse_iterator
    { return end(); }

    constexpr auto rend() const noexcept -> const_reverse_iterator
    { return begin(); }

    constexpr auto crbegin() const noexcept -> const_reverse_iterator
    { return rbegin(); }

    constexpr auto crend() const noexcept -> const_reverse_iterator
    { return rend(); }

    constexpr operator string_view_type() const noexcept
    { return {buffer_.data(), size_}; }

    constexpr auto c_str() const noexcept -> const char*
    {
        buffer_[size_] == '\0';
        return buffer_.data();
    }

    auto str() const -> std::string
    { return  {buffer_.data(), size_}; }

    template <std::size_t N1, typename CSDef>
    constexpr bool operator==(const basic_control_sequence<value_type, CSDef, N>& that) const
    { return string_view_type(*this) == string_view_type(*that); }

    constexpr bool operator==(string_view_type that) const
    { return string_view_type(*this) == that; }

    template <std::size_t N1, typename CSDef>
    constexpr std::weak_ordering operator<=>(const basic_control_sequence<value_type, CSDef, N>& that) const
    { return std::compare_weak_order_fallback(string_view_type(*this), string_view_type(that)); }

    constexpr std::weak_ordering operator<=>(string_view_type that) const
    { return std::compare_weak_order_fallback(string_view_type(*this), that); }

    // maximum required space:
    //      max 2 char : control_sequence
    //          1 char : param introducer (private sequences)
    //          1 char : null byte
    //
    //          5 chars per argument (max 5 digits: enough for all 16 bit values (max recognised by xterm)
    //          + 1 char per arg-1
    //          1 char : null byte
private:
    mutable std::array<char, capacity> buffer_;
    size_type size_;
};

//template <typename CharT, typename Def, typename... Ts>
//basic_control_sequence(detail::control_sequence_definition_tag<CSDef>, Ts... parameters) ->
//basic_control_sequence<CharT, CSDef>;

template <typename CSDef>
using control_sequence = basic_control_sequence<char, CSDef>;

#ifdef __cpp_lib_char8_t
template <control_sequence_definition CSDef, std::size_t N = detail::max_formatted_size_v<CSDef>>
using u8control_sequence = basic_control_sequence<char8_t, CSDef, N>;
#endif


template <typename CharT, typename CSDef, std::size_t N, typename Traits=std::char_traits<CharT>>
auto operator<<(std::basic_ostream<CharT, Traits>& os,
                basic_control_sequence<std::type_identity_t<CharT>, CSDef, N, Traits> s)
  -> std::basic_ostream<CharT, Traits>&
{
    os.write(s.data(), s.size());
    return os;
}

} // namespace termcontrol;
