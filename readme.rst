termcontrol
===========

A small C++20 library for ANSI control sequences and terminal bits.

Examples
--------

All examples assume :code:`namespace tc = termcontrol` for brevity.

Generate or print escape codes.

.. code:: cpp

    // Generate a control sequence as std::string
    std::string erase_page_after = tc::format<tc::def::erase_in_page>(erase_in_page_mode::after);

    // Directly print a control sequence to an output stream or output iterator.
    tc::format_to<tc::def::erase_in_page>(std::cout, erase_in_page_mode::after);

    // Parameters that are specific to a control sequence can be printed without needing to
    // specify the control_sequence_definition by using operator<<
    std::cout << erase_in_page_mode::after;

    // Make cursor invisible
    std::cout << tc::def::reset_mode(dec_mode::cursor_visible);


Create compile time escape codes.

.. code:: cpp

    constexpr auto move_up_10 = tc::control_sequence<tc::def::cursor_up>(10);
    constexpr auto erase_page_after = tc::control_sequence<tc::def::erase_in_page>(erase_in_page_mode::after);



Format styled text.

.. code:: cpp

    // underlined with red foregroung
    std::cout << em(tc::emphasis::underline) | fg(tc::terminal_color::red)
              << "Test"
              << text_style::reset();

    // equivalent command without temporaries.
    tc::format_to(std::cout, em(termcontrol::emphasis::underline) | fg(termcontrol::terminal_color::red, "Test");

Colors can be specified with predefined color schemes :code:`css_color`,
:code:`x11_color` and :code:`pwc_color`, or as :code:`rgb` values.

.. code:: cpp

    auto style1 = fg(tc::css_color::lightslategray);
    auto style2 = fg(tc::x11_color::light_goldenrod_yellow);
    auto style3 = fg(tc::pwc_color::light_brown);
    auto style4 = fg(tc::rgb(250,250,250))
