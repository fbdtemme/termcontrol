//
// Created by fbdtemme on 13/01/2020.
//

#include <catch2/catch.hpp>
#include <iostream>
#include <string_view>

#include "detail/color.hpp"
#include "detail/format.hpp"
#include "detail/definitions.hpp"

//
//TEST_CASE("check definition helper traits")
//{
//    using namespace termcontrol;
//    static_assert(detail::is_overloaded_sequence_def<overload_set<argument_pack<termcontrol::mode>, termcontrol::dec_mode>>::value);
//    static_assert(!detail::is_valid_overloaded_sequence_def<overload_set<argument_pack<termcontrol::mode>,
//            termcontrol::dec_mode>>::value);
//}
//
//TEST_CASE("check access helper traits")
//{
//    using namespace termcontrol;
//    const auto i = detail::overload_set_size<set_mode>::value;
//    CHECK(i == 2);
//    using T = detail::overload_set_element<0, set_mode>::type;
//    CHECK(std::is_same_v<T, std::tuple<termcontrol::mode>>);
//}
//
////
////TEST_CASE("test default arguments definition")
////{
////    using namespace termcontrol::experimental;
////    auto [p1] = default_arguments<cursor_up>::value;
////    CHECK(p1);
////    CHECK(p1 == 1);
////}
////
////


namespace termcontrol { namespace cf = termcontrol::control_functions; };

TEST_CASE("test default arguments call")
{
    using namespace termcontrol;

    std::array<char, 20> buf {};
    auto out = detail::format_to(cf::cursor_up, buf.begin());
    auto size = std::distance(buf.begin(), out);
    std::cout << std::string_view(buf.data(), size);
}
////
////
////TEST_CASE("blah")
////{
////    using namespace termcontrol::experimental;
//////    CHECK(is_overloaded_sequence_def<termcontrol::experimental::set_mode>::value);
////    bool v = detail::valid_arguments_for_dispatch<termcontrol::experimental::set_mode, termcontrol::mode>();
////    CHECK(v);
////}
//
//
//template < typename D, typename Buffer, typename... T>
//constexpr auto format_helper(Buffer& buffer, T&&... arg) -> std::string_view
//{
//    auto out = termcontrol::detail::format_control_sequence<D>(buffer.begin(), std::forward<T>(arg)...);
//    return std::string_view(buffer.begin(), std::distance(buffer.begin(), out));
//}
//
//
//TEST_CASE("test format_control_sequence")
//{
//    using namespace termcontrol;
//    std::array<char, 40> buf {};
//
//    SECTION("callable for 1 default argument") {
//        const auto v1 = format_helper<character_position_absolute>(buf);
//        CHECK(v1 == "\033[1`");
//    }
//
//    SECTION("callable for 2 default argument") {
//        const auto v1 = format_helper<cursor_position>(buf);
//        CHECK(v1 == "\033[1;1H");
//    }
//
//    SECTION("callable select_graphics_rendition") {
//        using namespace termcontrol;
//        const auto v1 = format_helper<select_graphics_rendition>(
//                buf, em(emphasis::double_underline) | fg(terminal_color::red));
//        CHECK(v1 == "\033[21;31m");
//    }
//
//    SECTION("callable for overloaded definitions") {
//        const auto v1 = format_helper<set_mode>(buf, termcontrol::mode::character_editing);
//        CHECK(v1 == "\033[10h");
//        const auto v2 = format_helper<set_mode>(buf, termcontrol::dec_mode::x11_mouse_reporting);
//        CHECK(v2 == "\033[?1000h");
//    }
//}