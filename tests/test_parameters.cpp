////
//// Created by fbdtemme on 03/01/2020.
////
#include <catch2/catch.hpp>

#include <termcontrol/termcontrol.hpp>

#include <random>
#include <charconv>

using namespace termcontrol;
using namespace termcontrol::detail;

template <typename Buffer, typename T>
constexpr auto format_helper(Buffer& buffer, T&& arg) -> std::string_view
{
    auto out = termcontrol::detail::format_parameter(buffer.begin(), std::forward<T>(arg));
    return std::string_view(buffer.begin(), std::distance(buffer.begin(), out));
}

static std::array<char, 200> buffer{};

template <typename T>
auto to_characters(T value)
{
    auto [p, ecc] = std::to_chars(buffer.begin(), buffer.end(), value, 10);
    return std::string_view(buffer.begin(), std::distance(buffer.begin(), p));
}


TEMPLATE_TEST_CASE("Test format_int", "", std::uint8_t, std::uint16_t, std::uint32_t, std::uint64_t,
                                          std::int8_t, std::int16_t, std::int32_t, std::int64_t)
{
    using namespace Catch::Generators;

    const TestType min = std::numeric_limits<TestType>::min();
    const TestType zero = 0;
    const TestType max = std::numeric_limits<TestType>::max();

    CHECK(std::string_view(format_int(min)) ==  to_characters(min));
    CHECK(std::string_view(format_int(zero)) == to_characters(zero));
    CHECK(std::string_view(format_int(max)) ==  to_characters(max));

    auto i = GENERATE_REF(take(100, random(min, max)));
    CHECK(std::string_view(format_int(i)) == to_characters(i));
}



TEST_CASE("test erase_line_mode")
{
    using namespace termcontrol;

    SECTION("format_parameter") {
        CHECK(format_helper(buffer, erase_line_mode::after)  == "0");
        CHECK(format_helper(buffer, erase_line_mode::before) == "1");
        CHECK(format_helper(buffer, erase_line_mode::full)   == "2");
    }

    SECTION("max_formatted_size") {
        CHECK(max_formatted_size_v<erase_line_mode> == 1);
    }
}

TEST_CASE("test erase_page_mode")
{
    using namespace termcontrol;

    SECTION("format_parameter") {
        CHECK(format_helper(buffer, erase_page_mode::below)  == "0");
        CHECK(format_helper(buffer, erase_page_mode::above) == "1");
        CHECK(format_helper(buffer, erase_page_mode::full)   == "2");
        CHECK(format_helper(buffer, erase_page_mode::full_scrollback)   == "3");
    }

    SECTION("max_formatted_size") {
        CHECK(max_formatted_size_v<erase_page_mode> == 1);
    }
}

TEST_CASE("test erase_field_mode")
{
    using namespace termcontrol;

    SECTION("format_parameter") {
        CHECK(format_helper(buffer, erase_in_field_mode::after)  == "0");
        CHECK(format_helper(buffer, erase_in_field_mode::before) == "1");
        CHECK(format_helper(buffer, erase_in_field_mode::full)   == "2");
    }

    SECTION("max_formatted_size") {
        CHECK(max_formatted_size_v<erase_in_field_mode> == 1);
    }
}


TEST_CASE("test erase_area_mode")
{

    SECTION("format_parameter") {
        CHECK(format_helper(buffer, erase_in_area_mode::after)  == "0");
        CHECK(format_helper(buffer, erase_in_area_mode::before) == "1");
        CHECK(format_helper(buffer, erase_in_area_mode::full)   == "2");
    }

    SECTION("max_formatted_size") {
        CHECK(max_formatted_size_v<erase_in_area_mode> == 1);
    }
}



TEST_CASE("test tab_control_mode")
{
    using tcm = cursor_tabulation_control_mode;

    SECTION("format_parameter") {
        CHECK(format_helper(buffer, tcm::set_character_tab) == "0");
        CHECK(format_helper(buffer, tcm::set_line_tab) == "1");
        CHECK(format_helper(buffer, tcm::clear_character_tab) == "2");
        CHECK(format_helper(buffer, tcm::clear_line_tab) == "3");
        CHECK(format_helper(buffer, tcm::clear_character_tab_line) == "4");
        CHECK(format_helper(buffer, tcm::clear_character_tab_all) == "5");
        CHECK(format_helper(buffer, tcm::clear_line_tab_all) == "6");

        SECTION("two flag combinations") {
            auto t1 = tcm::set_character_tab | tcm::set_line_tab;
            CHECK(format_helper(buffer, t1) == "0;1");

            auto t2 = tcm::set_character_tab | tcm::clear_character_tab;
            CHECK_THROWS_AS(format_helper(buffer, t2), std::invalid_argument);

            auto t3 = tcm::clear_character_tab | tcm::clear_character_tab_all;
            CHECK_THROWS_AS(format_helper(buffer, t3), std::invalid_argument);
        }
        SECTION("more than two flags") {
            auto t = tcm::set_character_tab | tcm::set_line_tab | tcm::clear_line_tab_all;
            CHECK_THROWS_AS(format_helper(buffer, t), std::invalid_argument);
        }
    }

    SECTION("max_formatted_size") {
        CHECK(max_formatted_size_v<tcm> == 3);
    }
}

//TEST_CASE("test formatted_size")
//{
//    using namespace termcontrol;
//    using namespace termcontrol::detail;
//    auto v = std::numeric_limits<std::uint8_t>::digits10;
//    CHECK(max_formatted_size_v<std::uint8_t> == 3);
//    CHECK(max_formatted_size_v<std::uint16_t> == 5);
//    CHECK(max_formatted_size_v<std::uint32_t> == 10);
//    CHECK(max_formatted_size_v<std::uint64_t> == 20);
//    CHECK(max_formatted_size_v<std::int8_t> == 3);
//    CHECK(max_formatted_size_v<std::int16_t> == 5);
//    CHECK(max_formatted_size_v<std::int32_t> == 10);
//    CHECK(max_formatted_size_v<std::int64_t> == 19);
//
//    CHECK(max_formatted_size_v<erase_line_mode> == 1);
//    CHECK(max_formatted_size_v<erase_page_mode> == 1);
//    CHECK(max_formatted_size_v<tab_clear_mode> == 1);
//    CHECK(max_formatted_size_v<mode> == 2);
//    CHECK(max_formatted_size_v<dec_mode> == 4);
//}
//
//



//TEST_CASE("test format_helper")
//{
//    using namespace termcontrol;
//
//    SECTION("enums - erase_line_mode")
//    {
//
//    }
//
//
//    SECTION("rgb_color") {
//        SECTION("rgb_color{ 255, 255, 255 }"){
//            CHECK(format_helper(buffer, rgb_color{255, 255, 255}) == "255;255;255");
//        }
//        SECTION("rgb_color{ 0, 0, 0 }"){
//            CHECK(format_helper(buffer, rgb_color{0, 0, 0}) == "0;0;0");
//        }
//    }
//
//    SECTION("emphasis") {
//        CHECK(format_helper(buffer, emphasis::reset) == "0");
//        CHECK(format_helper(buffer, emphasis::bold)  == "1");
//        CHECK(format_helper(buffer, emphasis::italic)    == "3");
//        CHECK(format_helper(buffer, emphasis::underline) == "4");
//        CHECK(format_helper(buffer, emphasis::striketrough)  == "9");
//        CHECK(format_helper(buffer, emphasis::overline)  == "53");
//        CHECK(format_helper(buffer, emphasis::double_underline)  == "21");
//        CHECK(format_helper(buffer, emphasis::blinking)  == "5");
//        CHECK(format_helper(buffer, emphasis::reverse)   == "7");
//    }
//}
//

